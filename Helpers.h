#pragma once
#include <math.h>

long long powsum(long long a, long long b) {
	return pow(a + b, 2);
}